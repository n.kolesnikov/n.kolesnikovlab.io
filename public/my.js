function updatePrice()
{
  let s = document.getElementsByName("prodType");
  let select = s[0];
  let price = 0;
  let q;
  q = parseInt(document.calk.quantity.value);
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.prodTypes[priceIndex];
}
  let radioDiv = document.getElementById("radios");
  if(select.value=="1")
  {
    radioDiv.style.display ="none";
  }
  if(select.value=="2")
  {
    radioDiv.style.display ="block";
  }
  if(select.value=="3")
  {
    radioDiv.style.display ="none";
  }
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.prodOptions[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });
  let checkDiv = document.getElementById("checkboxes");
  if(select.value=="1")
  {
    checkDiv.style.display ="none";
  }
  if(select.value=="2")
  {
    checkDiv.style.display ="none";
  }
  if(select.value=="3")
  {
    checkDiv.style.display ="block";
  }
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    if (checkbox.checked) {
      let propPrice = prices.prodProperties[checkbox.name];
      if (propPrice !== undefined) {
        price += propPrice;
      }
    }
  });

  let prodPrice = document.getElementById("out");
  prodPrice.innerHTML =q*price + " рублей";
}
function getPrices()
{
  return {
    prodTypes: [420, 250, 330],
    prodOptions: {
      option1: 0,
      option2: 100,
    },
    prodProperties: {
      prop1: 50,
    }
  };
}
function rb(){
  var ele = document.getElementsByName("prodOptions");
   for(var i=0;i<ele.length;i++)
      ele[i].checked = false;
}
function rch(){
  var ele = document.getElementsByName("prop1");
   for(var i=0;i<ele.length;i++)
      ele[i].checked = false;
}
window.addEventListener("DOMContentLoaded", function (event)
{
  let s = document.getElementsByName("prodType");
  let select = s[0];
  select.addEventListener("change", function(event) {
    rb();
    rch();
    updatePrice();
  });
  document.calk.quantity.addEventListener("input", updatePrice);
  let radios = document.getElementsByName("prodOptions");
  radios.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      updatePrice();
    });
  });
  let checkboxes = document.querySelectorAll("#checkboxes input");
  checkboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      updatePrice();
    });
  });

  updatePrice();
});
